import 'package:app_sinbi_apartment/model/news/news_list.dart';
import 'package:flutter/material.dart';

class ComponentNewsList extends StatelessWidget {
  const ComponentNewsList(
      {super.key, required this.newsList, required this.callback});

  final NewsList newsList;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: callback,
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              height: 50,
              decoration: const BoxDecoration(
                  border: Border(
                bottom: BorderSide(color: Color.fromRGBO(223, 223, 223, 87)),
              )),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    newsList.title,
                    style: const TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 16,
                      letterSpacing: 1.0,
                    ),
                  ),
                  Text('${newsList.newsState}')
                ],
              ),
            ),
            Container(
              height: 8,
            )
          ],
        ));
  }
}
