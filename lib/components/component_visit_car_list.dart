import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/model/visitcar/visit_car_list_item.dart';
import 'package:app_sinbi_apartment/repository/repo_visit_car.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ComponentVisitCarList extends StatefulWidget {
  ComponentVisitCarList({super.key, required this.visitCarListItem});

  VisitCarListItem visitCarListItem;

  @override
  State<ComponentVisitCarList> createState() => _ComponentVisitCarListState();
}

class _ComponentVisitCarListState extends State<ComponentVisitCarList> {
  final DateFormat dateFormat = DateFormat('yyyy-MM-dd HH:mm:ss');

  Future<void> _delVisitCar(int visitCarId) async {
    await RepoVisitCar()
        .delVisitCar(widget.visitCarListItem.visitCarId)
        .then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '삭제되었습니다.',
        subTitle: res.msg, // 백엔드에서 반환받은 메세지 넣기
      ).call();
      Navigator.pop(context);
      Navigator.pop(context, [true]);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '삭제 실패',
        subTitle: '삭제에 실패하였습니다. 다시 한번 시도해주세요',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        height: 80,
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(color: Color.fromRGBO(223, 223, 223, 87)))),
        margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
        //padding: const EdgeInsets.all(30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    widget.visitCarListItem.carNum,
                    style: const TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 18,
                      letterSpacing: 1.0,
                    ),
                  ),
                  const SizedBox(width: 10),
                  Text('(${widget.visitCarListItem.approvalState})'),
                  const SizedBox(width: 150),
                  if (widget.visitCarListItem.approvalState == '처리전')
                    TextButton(
                        onPressed: () {
                          _showDialog();
                        },
                        child: Text(
                          '취소하기',
                          style: TextStyle(color: Colors.red),
                        ))
                ],
              ),
            ),
            const SizedBox(height: 5),
            Container(
              margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
              child: Row(
                children: [
                  Text(dateFormat.format(widget.visitCarListItem.dateCreate),
                      style: const TextStyle(color: Colors.grey, fontSize: 12))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _showDialog() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            content: const Text('정말 취소하시겠습니까?'),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('취소')),
              TextButton(
                  onPressed: () {
                    _delVisitCar(widget.visitCarListItem.visitCarId);
                  },
                  child: const Text('확인')),
            ],
          );
        });
  }
}
