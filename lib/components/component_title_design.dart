import 'package:flutter/material.dart';

class ComponentTitleDesign extends StatelessWidget {
  const ComponentTitleDesign({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(10, 50, 0, 0),
      child: Row(
        children: [
          Text(
            title,
            style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 25,
                letterSpacing: 1.5,
                fontFamily: 'cafe'),
          )
        ],
      ),
    );
  }
}
