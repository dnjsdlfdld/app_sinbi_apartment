import 'package:app_sinbi_apartment/model/myhome/house_member_name_list.dart';
import 'package:app_sinbi_apartment/model/myhome/my_info_item.dart';
import 'package:flutter/material.dart';

class ComponentMyInfoList extends StatelessWidget {
  ComponentMyInfoList({super.key, required this.houseMemberNameList});

  final HouseMemberNameList houseMemberNameList;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
      height: 100,
      decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
      child: Row(
        children: [Text('세대원 : ${houseMemberNameList.houseMemberName}')],
      ),
    );
  }
}
