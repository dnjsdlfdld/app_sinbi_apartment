import 'package:app_sinbi_apartment/model/myhome/my_car_list.dart';
import 'package:flutter/material.dart';

class ComponentMyCarList extends StatelessWidget {
  const ComponentMyCarList({super.key, required this.myCarList});

  final MyCarList myCarList;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
          //width: 300,
          height: 100,
          decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(15)),
          child: Container(
            margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  children: [
                    const Text('차량번호 : ',
                        style: TextStyle(fontSize: 16, letterSpacing: 1.2)),
                    Text('${myCarList.myCarNum} ',
                        style: TextStyle(fontSize: 16, letterSpacing: 1.2))
                  ],
                ),
                const SizedBox(height: 5),
                Row(
                  children: [
                    const Text('차량모델 : ',
                        style: TextStyle(fontSize: 16, letterSpacing: 1.2)),
                    Text('${myCarList.carModel} ',
                        style: TextStyle(fontSize: 16, letterSpacing: 1.2))
                  ],
                ),
                const SizedBox(height: 5),
                Row(
                  children: [
                    const Text('차량타입 : ',
                        style: TextStyle(fontSize: 16, letterSpacing: 1.2)),
                    Text('${myCarList.carType} ',
                        style: TextStyle(fontSize: 16, letterSpacing: 1.2))
                  ],
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: 20)
      ],
    );
  }
}
