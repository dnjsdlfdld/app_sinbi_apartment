import 'package:app_sinbi_apartment/model/meeting/meeting_category_item.dart';
import 'package:flutter/material.dart';

class ComponentMeetingCategory extends StatelessWidget {
  const ComponentMeetingCategory({super.key, required this.meetingCategoryItem, required this.callback});

  final MeetingCategoryItem meetingCategoryItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 200,
                width: 350,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3),
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    Container(
                      width: 350,
                      height: 120,
                      child: Image.asset('${meetingCategoryItem.imgUrl}', fit: BoxFit.fill),
                      padding: const EdgeInsets.all(0),
                    ),
                    Container(
                      height: 80,
                      width: 350,
                      decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(bottomRight: Radius.circular(12), bottomLeft: Radius.circular(12))
                      ),
                      child: Column(
                        children: [
                          Text(
                            '${meetingCategoryItem.categoryName}',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w600,
                              letterSpacing: 1.5,
                              fontFamily: 'cafe',
                            ),
                          ),
                          SizedBox(height: 10),
                          Text(
                            '${meetingCategoryItem.categoryIntroduce}',
                            style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w400,
                              letterSpacing: 1.5,
                              fontFamily: 'cafe',
                            ),
                          ),
                        ],
                      ),
                      padding: const EdgeInsets.all(12),
                    )
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 20)
        ],
      ),
    );
  }
}
