import 'package:app_sinbi_apartment/model/complain/complain_comments_item.dart';
import 'package:flutter/material.dart';

class ComponentComplainDetailComments extends StatelessWidget {
  const ComponentComplainDetailComments(
      {super.key, required this.commentsItem});

  final ComplainCommentsItem commentsItem;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 60,
          width: 400,
          margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
          // decoration: BoxDecoration(
          //     border: Border(bottom: BorderSide(color: Color.fromRGBO(223, 223, 223, 87)))),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                children: [
                  Text('${commentsItem.content}',
                      style: TextStyle(fontSize: 15)),
                ],
              ),
              const SizedBox(height: 3),
              Row(
                children: [
                  Text('${commentsItem.residentFullName}',
                      style: TextStyle(color: Colors.grey, fontSize: 12)),
                ],
              )
            ],
          ),
        ),
        Container(height: 5, child: Divider(thickness: 1))
      ],
    );
  }
}
