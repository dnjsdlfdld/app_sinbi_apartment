import 'package:app_sinbi_apartment/model/complain/complain_comments_item.dart';
import 'package:app_sinbi_apartment/model/meeting/meeting_comments_list_item.dart';
import 'package:flutter/material.dart';

class ComponentMeetingComments extends StatelessWidget {
  const ComponentMeetingComments(
      {super.key, required this.meetingCommentsListItem});

  final MeetingCommentsListItem meetingCommentsListItem;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 60,
          width: 400,
          margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
          // decoration: BoxDecoration(
          //     border: Border(bottom: BorderSide(color: Color.fromRGBO(223, 223, 223, 87)))),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                children: [
                  Text('${meetingCommentsListItem.content}',
                      style: TextStyle(fontSize: 15)),
                ],
              ),
              const SizedBox(height: 3),
              Row(
                children: [
                  Text('${meetingCommentsListItem.residentFullName}',
                      style: TextStyle(color: Colors.grey, fontSize: 12)),
                ],
              )
            ],
          ),
        ),
        Container(height: 5, child: Divider(thickness: 1))
      ],
    );
  }
}
