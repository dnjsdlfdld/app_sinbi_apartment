import 'package:app_sinbi_apartment/model/meeting/meeting_list_item.dart';
import 'package:flutter/material.dart';

class ComponentMeetingList extends StatelessWidget {
  const ComponentMeetingList(
      {super.key, required this.meetingListItem, required this.callback});

  final MeetingListItem meetingListItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: callback,
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              height: 50,
              decoration: const BoxDecoration(
                  border: Border(
                    bottom: BorderSide(color: Color.fromRGBO(223, 223, 223, 87)),
                  )),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    meetingListItem.title,
                    style: const TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 16,
                      letterSpacing: 1.0,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 8,
            )
          ],
        ));
  }
}
