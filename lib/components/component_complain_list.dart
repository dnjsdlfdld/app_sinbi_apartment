import 'package:app_sinbi_apartment/config/config_color.dart';
import 'package:app_sinbi_apartment/model/complain/complain_list_item.dart';
import 'package:flutter/material.dart';

class ComponentComplainList extends StatelessWidget {
  const ComponentComplainList(
      {super.key, required this.complainListItem, required this.callback});

  final ComplainListItem complainListItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: callback,
        child: Column(
          children: [
            Container(
              height: 50,
              decoration: const BoxDecoration(
                  border: Border(
                bottom: BorderSide(color: Color.fromRGBO(223, 223, 223, 87)),
              )),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    complainListItem.title,
                    style: const TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 16,
                      letterSpacing: 1.0,
                    ),
                  ),
                  Text('${complainListItem.serviceState}')
                ],
              ),
            ),
            Container(
              height: 8,
            )
          ],
        ));
  }
}
