import 'package:flutter/material.dart';

class ComponentSubtitleDesign extends StatelessWidget {
  const ComponentSubtitleDesign({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 50, 0, 0),
      child: Row(
        children: [
          Text(
            title,
            style: TextStyle(
                fontWeight: FontWeight.w500, fontSize: 20, letterSpacing: 1.5),
          )
        ],
      ),
    );
  }
}
