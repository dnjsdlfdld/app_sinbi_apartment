import 'package:app_sinbi_apartment/pages/complain/page_complain.dart';
import 'package:app_sinbi_apartment/pages/complain/page_tab_view_received.dart';
import 'package:app_sinbi_apartment/pages/news/page_news.dart';
import 'package:app_sinbi_apartment/pages/page_index.dart';
import 'package:flutter/material.dart';

class ComponentMainText extends StatefulWidget {
  const ComponentMainText({Key? key}) : super(key: key);

  @override
  State<ComponentMainText> createState() => _ComponentMainTextState();
}

class _ComponentMainTextState extends State<ComponentMainText> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(left: 60),
          child: Row(
            children: [
              const Text('우리아파트',
                  style: TextStyle(fontSize: 40, fontWeight: FontWeight.w200)),
            ],
          ),
        ),
        const SizedBox(height: 10),
        Container(
          margin: EdgeInsets.only(left: 60),
          child: Row(
            children: const [
              Text('소식',
                  style: TextStyle(fontSize: 40, fontWeight: FontWeight.w600)),
              Text('과',
                  style: TextStyle(fontSize: 40, fontWeight: FontWeight.w200)),
              Text('민원',
                  style: TextStyle(fontSize: 40, fontWeight: FontWeight.w600)),
              Text('을',
                  style: TextStyle(fontSize: 40, fontWeight: FontWeight.w200)),
            ],
          ),
        ),
        const SizedBox(height: 10),
        Container(
          margin: EdgeInsets.only(left: 60),
          child: Row(
            children: [
              const Text('여기에서!',
                  style: TextStyle(fontSize: 40, fontWeight: FontWeight.w200)),
              SizedBox(width: 60),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(right: 50),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Image.asset('assets/sinbi1.jpg', width: 100, height: 100),
            ],
          ),
        ),
        const SizedBox(height: 30),
        Container(
          decoration: BoxDecoration(
            color: const Color.fromRGBO(178, 220, 90, 60),
            borderRadius: BorderRadius.circular(50),
          ),
          height: 240,
          width: 300,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.fromLTRB(25, 0, 0, 0),
                child: const Text('민원 창구',
                    style: TextStyle(
                        fontSize: 35,
                        color: Colors.white,
                        letterSpacing: 2,
                        fontWeight: FontWeight.w700)),
              ),
              const SizedBox(height: 30),
              Container(
                  margin: const EdgeInsets.fromLTRB(25, 0, 0, 0),
                  child: const Text('민원 창구 게시판입니다. \n아파트 민원만 작성해 주세요.',
                      style: TextStyle(
                        fontSize: 18,
                        letterSpacing: 1.2,
                        fontWeight: FontWeight.w400,
                      ))),
              SizedBox(height: 20),
              Container(
                margin: EdgeInsets.fromLTRB(25, 0, 0, 0),
                child: OutlinedButton(
                    style: OutlinedButton.styleFrom(
                        backgroundColor: Color.fromRGBO(216, 214, 214, 85),
                        primary: Colors.black,
                        side: BorderSide(
                            color: Color.fromRGBO(216, 214, 214, 85))),
                    onPressed: () async {
                      await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const PageComplain()));
                    },
                    child: Text('입장하기')),
              )
            ],
          ),
        ),
        const SizedBox(height: 50),
        Container(
          decoration: BoxDecoration(
            color: const Color.fromRGBO(238, 238, 106, 93),
            borderRadius: BorderRadius.circular(50),
          ),
          height: 240,
          width: 300,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.fromLTRB(25, 0, 0, 0),
                child: const Text('아파트 소식',
                    style: TextStyle(
                        fontSize: 35,
                        color: Colors.white,
                        letterSpacing: 2,
                        fontWeight: FontWeight.w700)),
              ),
              const SizedBox(height: 30),
              Container(
                  margin: const EdgeInsets.fromLTRB(25, 0, 0, 0),
                  child: const Text('우리 아파트의 다양한 소식들을 \n놓치지 말고 확인하세요.',
                      style: TextStyle(
                        fontSize: 18,
                        letterSpacing: 1.2,
                        fontWeight: FontWeight.w400,
                      ))),
              SizedBox(height: 20),
              Container(
                margin: EdgeInsets.fromLTRB(25, 0, 0, 0),
                child: OutlinedButton(
                    style: OutlinedButton.styleFrom(
                        backgroundColor: Color.fromRGBO(216, 214, 214, 85),
                        primary: Colors.black,
                        side: BorderSide(
                            color: Color.fromRGBO(216, 214, 214, 85))),
                    onPressed: () async {
                      await Navigator.push(context,
                          MaterialPageRoute(builder: (context) => PageNews()));
                    },
                    child: Text('입장하기')),
              )
            ],
          ),
        )
      ],
    );
  }
}
