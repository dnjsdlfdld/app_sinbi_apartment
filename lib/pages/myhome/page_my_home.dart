import 'package:app_sinbi_apartment/components/component_cupertino.dart';
import 'package:app_sinbi_apartment/components/component_my_car_list.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/components/component_subtitle_design.dart';
import 'package:app_sinbi_apartment/components/component_title_design.dart';
import 'package:app_sinbi_apartment/functions/token_lib.dart';
import 'package:app_sinbi_apartment/model/myhome/admin_cost_item.dart';
import 'package:app_sinbi_apartment/model/myhome/house_member_name_list.dart';
import 'package:app_sinbi_apartment/model/myhome/my_car_list.dart';
import 'package:app_sinbi_apartment/model/myhome/my_info_item.dart';
import 'package:app_sinbi_apartment/repository/repo_my_home.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PageMyHome extends StatefulWidget {
  const PageMyHome({Key? key}) : super(key: key);

  @override
  State<PageMyHome> createState() => _PageMyHomeState();
}

class _PageMyHomeState extends State<PageMyHome> {
  String getToday() {
    DateTime _today = DateTime.now();
    DateFormat format = DateFormat('MM');
    var strToday = format.format(_today);
    return strToday;
  }

  List<MyCarList> _listCar = [];
  List<HouseMemberNameList> _list = [];
  AdminCostItem _adminCostItem = AdminCostItem(0, 0);
  MyInfoItem _infoItem = MyInfoItem('', '', '');
  final _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _loadList();
    _loadItem();
    _loadMyCar();
    _loadAdminCost();
  }

  Future<void> _logout(BuildContext context) async {
    // functions/token_lib.dart 에서 만든 로그아웃 기능 호출
    TokenLib.logout(context);
  }

  Future<void> _loadMyCar() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoMyHome().getCarList().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _listCar = res.list;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '우리집 차량 로딩실패',
        subTitle: '네트워크가 원활하지 않거나 \n 데이터가 없습니다.',
      ).call();
    });
  }

  Future<void> _loadList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoMyHome().getHouseMember().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '세대원 불러오기 실패',
        subTitle: '네트워크가 원활하지 않거나 \n 데이터가 없습니다.',
      ).call();
    });
  }

  Future<void> _loadItem() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoMyHome().getMyInfo().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _infoItem = res.data;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '내 정보 불러오기 실패',
        subTitle: '네트워크가 원활하지 않거나 \n 데이터가 없습니다.',
      ).call();
    });
  }

  Future<void> _loadAdminCost() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoMyHome().getAdminCost().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _adminCostItem = res.data;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '관리비 불러오기 실패',
        subTitle: '네트워크가 원활하지 않거나 \n 데이터가 없습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(children: [
          ComponentTitleDesign(title: '그랑시티자이'),
          ComponentSubtitleDesign(title: '내 정보'),
          _buildInfo(),
          ComponentSubtitleDesign(title: '우리집 차량 정보'),
          _buildMyCar(),
          ComponentSubtitleDesign(title: '관리비'),
          _buildAdminCost(),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              TextButton(
                onPressed: () {
                  _showDialog();
                },
                child: const Text('로그아웃',
                    style: TextStyle(
                        color: Colors.grey,
                        decoration: TextDecoration.underline,
                        decorationStyle: TextDecorationStyle.solid,
                        decorationThickness: 5)),
              ),
            ],
          ),
        ]),
      ),
      backgroundColor: Colors.white,
    );
  }

  Widget _buildInfo() {
    return Container(
      margin: const EdgeInsets.fromLTRB(20, 20, 20, 0),
      //width: 300,
      height: 150,
      decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(15)),
      child: Container(
        margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                Text('세대주 : ${_infoItem.residentName}',
                    style: TextStyle(fontSize: 16, letterSpacing: 1.2)),
              ],
            ),
            const SizedBox(height: 5),
            Row(
              children: [
                const Text('세대원 : ',
                    style: TextStyle(fontSize: 16, letterSpacing: 1.2)),
                for (int i = 0; i < _list.length; i++)
                  Text('${_list[i].houseMemberName} ',
                      style: TextStyle(fontSize: 16, letterSpacing: 1.2))
              ],
            ),
            const SizedBox(height: 5),
            Row(
              children: [
                Text('연락처 : ${_infoItem.phone}',
                    style: TextStyle(fontSize: 16, letterSpacing: 1.2)),
              ],
            ),
            const SizedBox(height: 5),
            Row(
              children: [
                Container(
                    child: Text('주소 : ',
                        style: TextStyle(fontSize: 16, letterSpacing: 1.2))),
                Container(
                  width: 300,
                  child: Text(_infoItem.addressFullName,
                      style: TextStyle(fontSize: 16, letterSpacing: 1.2)),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildMyCar() {
    return ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: _listCar.length,
        itemBuilder: (_, index) => ComponentMyCarList(
              myCarList: _listCar[index],
            ));
  }

  Widget _buildAdminCost() {
    return Container(
      margin: const EdgeInsets.fromLTRB(20, 20, 20, 0),
      //width: 300,
      height: 150,
      decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(15)),
      child: Container(
        margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  children: [
                    Text('${getToday()}월 관리비 고지서',
                        style: TextStyle(fontSize: 18, letterSpacing: 1.2)),
                  ],
                ),
                const SizedBox(height: 30),
                Row(
                  children: [
                    Text(
                        _adminCostItem.price == 0 ? '추후 업데이트 예정입니다' : '${_adminCostItem.price}원',
                        style: TextStyle(
                            fontSize: 23,
                            //letterSpacing: 1.2,
                            fontWeight: FontWeight.w700))
                  ],
                ),
              ],
            ),
            //SizedBox(width: 80),
            // Row(
            //   children: [
            //     Image.asset('assets/sinbi5.png', width: 100, height: 100)
            //   ],
            // )
          ],
        ),
      ),
    );
  }

  void _showDialog() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            content: const Text('로그아웃하시겠습니까??'),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('취소')),
              TextButton(
                  onPressed: () {
                    _logout(context);
                  },
                  child: const Text('확인')),
            ],
          );
        });
  }
  void _loadCost() {
    if (_adminCostItem.price == null) {
      Text(
          '${_adminCostItem.price}원',
          style: TextStyle(
              fontSize: 28,
              letterSpacing: 1.2,
              fontWeight: FontWeight.w700));
    } else {
      Text('추후 업데이트 예정입니다.');
    }
  }
}
