import 'package:app_sinbi_apartment/components/component_cupertino.dart';
import 'package:app_sinbi_apartment/components/component_meeting_category.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/components/component_title_design.dart';
import 'package:app_sinbi_apartment/model/meeting/meeting_category_item.dart';
import 'package:app_sinbi_apartment/pages/meeting/page_meeting_list.dart';
import 'package:app_sinbi_apartment/repository/repo_meeting.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageMeetingCategory extends StatefulWidget {
  const PageMeetingCategory({Key? key}) : super(key: key);

  @override
  State<PageMeetingCategory> createState() => _PageMeetingCategoryState();
}

class _PageMeetingCategoryState extends State<PageMeetingCategory> {
  List<MeetingCategoryItem> _list = [];
  int _totalItemCount = 0;

  @override
  void initState() {
    super.initState();
    _loadCategory();
  }

  Future<void> _loadCategory() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoMeeting().getMeetingCategory().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list;
        _totalItemCount = res.totalItemCount;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '불러오기 실패',
        subTitle: '네트워크가 원활하지 않거나 \n 데이터가 없습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
          child: Column(
        children: [
          ComponentTitleDesign(title: '입주민 라운지'),
          SizedBox(height: 15),
          _buildBody()
        ],
      )),
    );
  }

  Widget _buildBody() {
    return Column(
      children: [
        ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentMeetingCategory(
                meetingCategoryItem: _list[index],
                callback: () async {
                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => PageMeetingList(
                                categoryName: _list[index].categoryName,
                                categoryIntroduce:
                                    _list[index].categoryIntroduce,
                                category: _list[index].category,
                              )));
                }))
      ],
    );
  }
}
