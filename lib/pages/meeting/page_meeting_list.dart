import 'package:app_sinbi_apartment/components/component_cupertino.dart';
import 'package:app_sinbi_apartment/components/component_meeting_list.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/components/component_title_design.dart';
import 'package:app_sinbi_apartment/model/meeting/meeting_list_item.dart';
import 'package:app_sinbi_apartment/pages/meeting/page_meeting_detail.dart';
import 'package:app_sinbi_apartment/repository/repo_meeting.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageMeetingList extends StatefulWidget {
  const PageMeetingList({super.key, required this.categoryName, required this.categoryIntroduce, required this.category});
  final String category;
  final String categoryName;
  final String categoryIntroduce;

  @override
  State<PageMeetingList> createState() => _PageMeetingListState();
}

class _PageMeetingListState extends State<PageMeetingList> {
  List<MeetingListItem> _list = [];
  int _totalItemCount = 0;

  @override
  void initState() {
    super.initState();
    _loadList(widget.category);
  }

  Future<void> _loadList(String category) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoMeeting().getMeetingList(widget.category).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list;
        _totalItemCount = res.totalItemCount;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '${widget.categoryName}불러오기 실패',
        subTitle: '네트워크가 원활하지 않거나 \n 데이터가 없습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            ComponentTitleDesign(title: '${widget.categoryName}'),
            SizedBox(height: 15),
            Container(
              margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
              child: Row(
                children: [
                  Icon(Icons.info_outline_rounded,
                      size: 12, color: Colors.blue),
                  SizedBox(width: 5),
                  Text(
                    '${widget.categoryIntroduce}',
                    style: TextStyle(color: Colors.grey, fontSize: 12),
                  ),
                ],
              ),
            ),
            _buildBody()
          ],
        ),
      ),
    );
  }

  Widget _buildBody() {
    return Column(
      children: [
        ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentMeetingList(
                meetingListItem: _list[index],
                callback: () async {
                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              PageMeetingDetail(meetingId: _list[index].id)));
                }))
      ],
    );
  }
}
