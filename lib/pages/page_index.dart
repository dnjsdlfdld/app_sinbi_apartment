import 'package:app_sinbi_apartment/pages/meeting/page_meeting_category.dart';
import 'package:app_sinbi_apartment/pages/myhome/page_my_home.dart';
import 'package:app_sinbi_apartment/pages/page_home.dart';
import 'package:app_sinbi_apartment/pages/visitcar/page_visit_car.dart';
import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  int _currentTabIndex = 0;

  @override
  Widget build(BuildContext context) {
    final _kTabPages = <Widget>[
      const PageHome(),
      const PageMeetingCategory(),
      const PageVisitCar(),
      const PageMyHome(),
    ];
    final _kBottmonNavBarItems = <BottomNavigationBarItem>[
      const BottomNavigationBarItem(icon: Icon(Icons.home_filled), label: '홈'),
      const BottomNavigationBarItem(
          icon: Icon(Icons.card_travel), label: 'Rounge'),
      const BottomNavigationBarItem(
          icon: Icon(Icons.local_taxi), label: '방문차량'),
      const BottomNavigationBarItem(
          icon: Icon(Icons.people_alt_sharp), label: '내정보'),
    ];
    assert(_kTabPages.length == _kBottmonNavBarItems.length);
    final bottomNavBar = BottomNavigationBar(
      unselectedFontSize: 11,
      selectedFontSize: 13,
      selectedItemColor: Colors.white,
      unselectedItemColor: Colors.grey,
      backgroundColor: Colors.black,
      elevation: 0,
      items: _kBottmonNavBarItems,
      currentIndex: _currentTabIndex,
      type: BottomNavigationBarType.fixed,
      onTap: (int index) {
        setState(() {
          _currentTabIndex = index;
        });
      },
    );
    return Scaffold(
      body: _kTabPages[_currentTabIndex],
      bottomNavigationBar: bottomNavBar,
    );
  }
}
