import 'package:app_sinbi_apartment/components/component_cupertino.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/components/component_title_design.dart';
import 'package:app_sinbi_apartment/config/config_form_validator.dart';
import 'package:app_sinbi_apartment/model/visitcar/visit_car_request.dart';
import 'package:app_sinbi_apartment/pages/visitcar/page_visit_car_list.dart';
import 'package:app_sinbi_apartment/repository/repo_visit_car.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';

class PageVisitCar extends StatefulWidget {
  const PageVisitCar({Key? key}) : super(key: key);

  @override
  State<PageVisitCar> createState() => _PageVisitCarState();
}

class _PageVisitCarState extends State<PageVisitCar> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _setVisitCar(VisitCarRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoVisitCar().setVisitCar(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '등록되었습니다.',
        subTitle: res.msg, // 백엔드에서 반환받은 메세지 넣기
      ).call();

      Navigator.pop(context, [true]);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '등록 실패',
        subTitle: '차량 등록에 실패하였습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          ComponentTitleDesign(title: '방문차량'),
          SizedBox(height: 100),
          ElevatedButton(
            onPressed: () {
              _showDialog();
            },
            child: Text('방문차량 신청'),
            style: ElevatedButton.styleFrom(
                textStyle: const TextStyle(fontSize: 20),
                minimumSize: Size(300, 60),
                primary: const Color.fromRGBO(55, 77, 146, 57)),
          ),
          const SizedBox(height: 50),
          ElevatedButton(
            onPressed: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => PageVisitCarList()));
            },
            child: Text('신청내역 확인'),
            style: ElevatedButton.styleFrom(
                textStyle: const TextStyle(fontSize: 20),
                minimumSize: Size(300, 60),
                primary: const Color.fromRGBO(55, 77, 146, 57)),
          ),
          SizedBox(height: 50),
          Container(
            margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
            height: 100,
            width: 300,
            decoration:
                BoxDecoration(border: Border.all(color: Colors.redAccent)),
            child: Container(
              margin: EdgeInsets.fromLTRB(7, 10, 7, 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      Text('등록되지 않은 차량은 입차 제한됩니다.',
                          style: TextStyle(fontSize: 14)),
                    ],
                  ),
                  SizedBox(height: 7),
                  Row(
                    children: [
                      Text('방문 일자를 정확하게 기입해 주세요.',
                          style: TextStyle(fontSize: 14)),
                    ],
                  ),
                  SizedBox(height: 7),
                  Row(
                    children: [
                      Text('기간 연장의 경우 경비실로 문의 바랍니다.',
                          style: TextStyle(fontSize: 14)),
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
      backgroundColor: const Color.fromRGBO(209, 240, 255, 100),
    );
  }

  void _showDialog() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('차량번호를 입력해주세요.'),
            content: FormBuilder(
              key: _formKey,
              child: Container(
                child: Column(mainAxisSize: MainAxisSize.min, children: [
                  FormBuilderTextField(
                    textInputAction: TextInputAction.next,
                    name: 'carNum',
                    decoration: const InputDecoration(
                        labelText: "차량번호", hintText: 'ex) 123가 1234'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(7,
                          errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(8,
                          errorText: formErrorMaxLength(20)),
                    ]),
                    keyboardType: TextInputType.text,
                  ),
                  FormBuilderDateTimePicker(
                    textInputAction: TextInputAction.next,
                    name: 'dateStart',
                    format: DateFormat('yyyy-MM-dd'),
                    inputType: InputType.date,
                    decoration: InputDecoration(
                      labelText: "시작일자",
                      suffixIcon: IconButton(
                        icon: const Icon(Icons.close),
                        onPressed: () {
                          _formKey.currentState!.fields['dateStart']
                              ?.didChange(null);
                        },
                      ),
                    ),
                    locale: const Locale.fromSubtags(languageCode: 'ko'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired)
                    ]),
                  ),
                  FormBuilderDateTimePicker(
                    textInputAction: TextInputAction.next,
                    name: 'dateEnd',
                    format: DateFormat('yyyy-MM-dd'),
                    inputType: InputType.date,
                    decoration: InputDecoration(
                      labelText: "종료일자",
                      suffixIcon: IconButton(
                        icon: const Icon(Icons.close),
                        onPressed: () {
                          _formKey.currentState!.fields['dateEnd']
                              ?.didChange(null);
                        },
                      ),
                    ),
                    locale: const Locale.fromSubtags(languageCode: 'ko'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired)
                    ]),
                  ),
                ]),
              ),
            ),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('취소')),
              TextButton(
                  onPressed: () {
                    if (_formKey.currentState?.saveAndValidate() ?? false) {
                      VisitCarRequest request = VisitCarRequest(
                        _formKey.currentState!.fields['carNum']!.value,
                        DateFormat('yyyy-MM-dd').format(
                            _formKey.currentState!.fields['dateStart']!.value),
                        DateFormat('yyyy-MM-dd').format(
                            _formKey.currentState!.fields['dateEnd']!.value),
                      );

                      _setVisitCar(request);
                    }
                  },
                  child: const Text('확인')),
            ],
          );
        });
  }
}
