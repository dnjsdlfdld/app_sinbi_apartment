import 'package:app_sinbi_apartment/components/component_count_title.dart';
import 'package:app_sinbi_apartment/components/component_cupertino.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/components/component_visit_car_list.dart';
import 'package:app_sinbi_apartment/model/visitcar/visit_car_list_item.dart';
import 'package:app_sinbi_apartment/repository/repo_visit_car.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PageVisitCarList extends StatefulWidget {
  const PageVisitCarList({super.key});

  @override
  State<PageVisitCarList> createState() => _PageVisitCarListState();
}

class _PageVisitCarListState extends State<PageVisitCarList> {
  final _scrollController = ScrollController();
  List<VisitCarListItem> _list = [];
  int _totalItemCount = 0;

  @override
  void initState() {
    super.initState();
    _loadVisitCarList();
  }

  Future<void> _loadVisitCarList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoVisitCar().getVisitCar().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list;
        _totalItemCount = res.totalItemCount;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '불러오기 실패',
        subTitle: '네트워크가 원활하지 않거나 \n 데이터가 없습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        iconTheme: const IconThemeData(color: Colors.black),
        centerTitle: true,
        title: Text(
          '신청내역',
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: ListView(
        controller: _scrollController,
        children: [
          SizedBox(height: 10),
          Container(
            margin: const EdgeInsets.fromLTRB(20, 0, 10, 0),
            child: ComponentCountTitle(
                icon: Icons.car_crash_rounded,
                count: _totalItemCount,
                unitName: '건',
                itemName: '신청내역'),
          ),
          SizedBox(height: 20),
          _buildBody(),
        ],
      ),
      backgroundColor: Colors.white,
    );
  }

  Widget _buildBody() {
    return Column(
      children: [
        ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) =>
                ComponentVisitCarList(visitCarListItem: _list[index]))
      ],
    );
  }
}
