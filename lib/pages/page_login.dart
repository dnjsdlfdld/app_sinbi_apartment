import 'package:app_sinbi_apartment/components/component_cupertino.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/components/component_title_design.dart';
import 'package:app_sinbi_apartment/config/config_form_validator.dart';
import 'package:app_sinbi_apartment/functions/token_lib.dart';
import 'package:app_sinbi_apartment/middleware_login_check/middleware_login_check.dart';
import 'package:app_sinbi_apartment/model/login/login_request.dart';
import 'package:app_sinbi_apartment/repository/repo_login.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageLogin extends StatefulWidget {
  const PageLogin({Key? key}) : super(key: key);

  @override
  State<PageLogin> createState() => _PageLoginState();
}

class _PageLoginState extends State<PageLogin> {
  bool obscurePassword = true;
  final _formKey = GlobalKey<FormBuilderState>();
  Pattern pattern = r'^[a-zA-Z]{1}[a-zA-Z0-9]{4,19}$';

  Future<void> _doLogin(LoginRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoLogin().doLogin(request).then((result) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '로그인 성공',
        subTitle: '로그인에 성공하였습니다.',
      ).call();

      TokenLib.setToken(result.data.token.toString());
      MiddlewareLoginCheck().check(context);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '로그인 실패',
        subTitle: '아이디 혹은 비밀번호를 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Column(
        children: [
          ComponentTitleDesign(title: '로그인'),
          FormBuilder(
              key: _formKey,
              autovalidateMode: AutovalidateMode.disabled,
              child: Column(
                children: [
                  SizedBox(height: 50),
                  Container(
                      margin: const EdgeInsets.fromLTRB(40, 0, 40, 0),
                      child: FormBuilderTextField(
                        textInputAction: TextInputAction.next,
                        style: const TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 15),
                        textAlign: TextAlign.center,
                        name: 'username',
                        validator: FormBuilderValidators.compose([
                          FormBuilderValidators.match(pattern.toString(),
                              errorText: formErrorMatchUsername),
                          FormBuilderValidators.required(
                              errorText: formErrorRequired),
                          FormBuilderValidators.minLength(8,
                              errorText: formErrorMinLength(8)),
                          FormBuilderValidators.maxLength(20,
                              errorText: formErrorMaxLength(20)),
                        ]),
                        decoration: const InputDecoration(
                            labelText: 'ID',
                            labelStyle: TextStyle(fontSize: 13),
                            hintText: '아이디를 입력해주세요.',
                            hintStyle: TextStyle(fontSize: 13)),
                        keyboardType: TextInputType.text,
                      )),
                  SizedBox(height: 30),
                  Container(
                      margin: const EdgeInsets.fromLTRB(40, 0, 40, 0),
                      child: FormBuilderTextField(
                        textInputAction: TextInputAction.next,
                        style: const TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 15),
                        textAlign: TextAlign.center,
                        name: 'password',
                        obscureText: obscurePassword,
                        obscuringCharacter: '●',
                        validator: FormBuilderValidators.compose([
                          FormBuilderValidators.required(
                              errorText: formErrorRequired),
                          FormBuilderValidators.minLength(8,
                              errorText: formErrorMinLength(8)),
                          FormBuilderValidators.maxLength(20,
                              errorText: formErrorMaxLength(20)),
                        ]),
                        decoration: InputDecoration(
                          labelText: 'PASSWORD',
                          labelStyle: TextStyle(fontSize: 13),
                          hintText: '비밀번호를 입력해주세요',
                          hintStyle: TextStyle(fontSize: 13),
                          suffixIcon: IconButton(
                              onPressed: () => setState(
                                  () => obscurePassword = !obscurePassword),
                              icon: Icon(
                                obscurePassword
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                                color: Colors.grey,
                                size: 20,
                              )),
                          prefix: Padding(padding: EdgeInsets.only(left: 50))
                        ),
                        keyboardType: TextInputType.text,
                      )),
                  const SizedBox(height: 20),
                  Container(
                      width: 335,
                      child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                            backgroundColor: Colors.lightBlue),
                        onPressed: () {
                          if (_formKey.currentState?.saveAndValidate() ??
                              false) {
                            LoginRequest request = LoginRequest(
                              _formKey.currentState!.fields['username']!.value,
                              _formKey.currentState!.fields['password']!.value,
                            );
                            _doLogin(request);
                          }
                        },
                        child: const Text('로그인',
                            style: TextStyle(color: Colors.white)),
                      )),
                  SizedBox(height: 30),
                  Container(
                    margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    height: 100,
                    width: 340,
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.redAccent)),
                    child: Container(
                      margin: EdgeInsets.fromLTRB(7, 10, 7, 10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            children: [
                              Text('상대방이 불쾌할 수 있는 내용은 삼가 바랍니다.',
                                  style: TextStyle(fontSize: 14)),
                            ],
                          ),
                          SizedBox(height: 7),
                          Row(
                            children: [
                              Text('타인의 계정을 무단 사용 시 제재 대상이 될 수 있습니다.',
                                  style: TextStyle(fontSize: 14)),
                            ],
                          ),
                          SizedBox(height: 7),
                          Row(
                            children: [
                              Text('계정 생성은 관리사무소로 문의 바랍니다.',
                                  style: TextStyle(fontSize: 14)),
                            ],
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ))
        ],
      ),
    );
  }
}
