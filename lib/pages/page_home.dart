import 'package:app_sinbi_apartment/components/component_cupertino.dart';
import 'package:app_sinbi_apartment/components/component_main_text.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/components/component_title_design.dart';
import 'package:app_sinbi_apartment/functions/token_lib.dart';
import 'package:app_sinbi_apartment/model/main_home/resident_name_item.dart';
import 'package:app_sinbi_apartment/repository/repo_main_home.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PageHome extends StatefulWidget {
  const PageHome({Key? key}) : super(key: key);

  @override
  State<PageHome> createState() => _PageHomeState();
}

class _PageHomeState extends State<PageHome> {
  ResidentNameItem _item = ResidentNameItem('');

  @override
  void initState() {
    super.initState();
    getName();
  }

  Future<void> getName() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoMainHome().getName().then((res) {
      BotToast.closeAllLoading();
      setState(() {
        _item = res.data;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '네트워크 연결 실패',
        subTitle: '인터넷 연결이 원활하지 않습니다. \n와이파이나 데이터 상태를 확인해 주세요',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            Row(
              children: const [
                ComponentTitleDesign(title: '그랑시티자이'),
              ],
            ),
            SizedBox(height: 5),
            Container(
              margin: const EdgeInsets.only(left: 10),
              child: Row(
                children: [
                  Text(
                    '\u{1f61a} 환영합니다 ${_item.name} 님',
                    style: TextStyle(fontFamily: 'cafe'),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 100),
            const ComponentMainText(),
          ],
        ),
      ),
      backgroundColor: Colors.white,
    );
  }
}
