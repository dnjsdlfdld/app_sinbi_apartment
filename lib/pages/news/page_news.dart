import 'package:app_sinbi_apartment/components/component_cupertino.dart';
import 'package:app_sinbi_apartment/components/component_news_list.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/components/component_title_design.dart';
import 'package:app_sinbi_apartment/model/news/news_list.dart';
import 'package:app_sinbi_apartment/pages/news/page_news_detail.dart';
import 'package:app_sinbi_apartment/repository/repo_news.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageNews extends StatefulWidget {
  const PageNews({Key? key}) : super(key: key);

  @override
  State<PageNews> createState() => _PageNewsState();
}

class _PageNewsState extends State<PageNews> {
  List<NewsList> _list = [];
  int _totalItemCount = 0;

  @override
  void initState() {
    super.initState();
    _loadList();
  }

  Future<void> _loadList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoNews().getList().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list;
        _totalItemCount = res.totalItemCount;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '불러오기 실패',
        subTitle: '네트워크가 원활하지 않거나 \n 데이터가 없습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            ComponentTitleDesign(title: '아파트 소식'),
            SizedBox(height: 15),
            Container(
              margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
              child: Row(
                children: [
                  Icon(Icons.info_outline_rounded,
                      size: 12, color: Colors.blue),
                  SizedBox(width: 5),
                  Text(
                    '우리 아파트의 다양한 소식들을 놓치지 말고 확인하세요.',
                    style: TextStyle(color: Colors.grey, fontSize: 12),
                  ),
                ],
              ),
            ),
            _buildBody()
          ],
        ),
      ),
    );
  }

  Widget _buildBody() {
    return Column(
      children: [
        ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentNewsList(
                newsList: _list[index],
                callback: () async {
                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              PageNewsDetail(newsId: _list[index].newsId)));
                }))
      ],
    );
  }
}
