import 'package:app_sinbi_apartment/components/component_cupertino.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/config/config_form_validator.dart';
import 'package:app_sinbi_apartment/model/complain/complain_request.dart';
import 'package:app_sinbi_apartment/repository/repo_complain.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageSetComplain extends StatefulWidget {
  const PageSetComplain({Key? key}) : super(key: key);

  @override
  State<PageSetComplain> createState() => _PageSetComplainState();
}

class _PageSetComplainState extends State<PageSetComplain> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _setComplain(ComplainRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoComplain().setData(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '민원이 등록되었습니다.',
        subTitle: res.msg, // 백엔드에서 반환받은 메세지 넣기
      ).call();

      Navigator.pop(context, [true]);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '등록 실패',
        subTitle: '민원 등록에 실패하였습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: true,
          iconTheme: const IconThemeData(color: Colors.black),
          centerTitle: true,
          title: Text('민원작성', style: TextStyle(color: Colors.black)),
          backgroundColor: Colors.white,
          elevation: 0,
        ),
        body: GestureDetector(
            onTap: () {
              FocusManager.instance.primaryFocus?.unfocus();
            },
            child: _buildBody()),
        backgroundColor: Colors.white,
        bottomSheet: Container(
          margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ElevatedButton(
            child: Text('등록'),
            style: ElevatedButton.styleFrom(
              minimumSize: Size(420, 40),
            ),
            onPressed: () {
              if (_formKey.currentState?.saveAndValidate() ?? false) {
                ComplainRequest request = ComplainRequest(
                  _formKey.currentState!.fields['title']!.value,
                  _formKey.currentState!.fields['content']!.value,
                );

                _setComplain(request);
              }
            },
          ),
          color: Colors.white,
        ));
  }

  Widget _buildBody() {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SingleChildScrollView(
          child: FormBuilder(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        key: _formKey,
        child: Container(
          margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
          child: Column(
            children: [
              const SizedBox(height: 30),
              Row(
                children: [
                  Text(
                    '제목',
                    style: TextStyle(
                        fontSize: 18,
                        letterSpacing: 1.5,
                        fontWeight: FontWeight.w600),
                  ),
                ],
              ),
              const SizedBox(height: 20),
              Container(
                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.circular(15)),
                child: FormBuilderTextField(
                  cursorColor: Colors.grey,
                  maxLines: 2,
                  minLines: 1,
                  textInputAction: TextInputAction.next,
                  name: 'title',
                  decoration: const InputDecoration(
                      hintText: '제목을 입력해 주세요.',
                      hintStyle: TextStyle(
                          fontWeight: FontWeight.w400, letterSpacing: 1.3),
                      enabledBorder:
                          UnderlineInputBorder(borderSide: BorderSide.none),
                      focusedBorder:
                          UnderlineInputBorder(borderSide: BorderSide.none),
                      contentPadding: EdgeInsets.symmetric(vertical: 10)),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                    FormBuilderValidators.minLength(2,
                        errorText: formErrorMinLength(2)),
                    FormBuilderValidators.maxLength(20,
                        errorText: formErrorMaxLength(20)),
                  ]),
                  keyboardType: TextInputType.text,
                ),
              ),
              const SizedBox(height: 30),
              Row(
                children: [
                  Text(
                    '내용',
                    style: TextStyle(
                        fontSize: 18,
                        letterSpacing: 1.5,
                        fontWeight: FontWeight.w600),
                  ),
                ],
              ),
              const SizedBox(height: 20),
              Container(
                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.circular(15)),
                child: FormBuilderTextField(
                  maxLines: 5,
                  minLines: 1,
                  cursorColor: Colors.grey,
                  textInputAction: TextInputAction.next,
                  name: 'content',
                  decoration: const InputDecoration(
                      hintText: '내용을 입력해 주세요.',
                      hintStyle: TextStyle(
                          fontWeight: FontWeight.w400, letterSpacing: 1.3),
                      enabledBorder:
                          UnderlineInputBorder(borderSide: BorderSide.none),
                      focusedBorder:
                          UnderlineInputBorder(borderSide: BorderSide.none),
                      contentPadding: EdgeInsets.symmetric(vertical: 50)),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                    FormBuilderValidators.minLength(1,
                        errorText: formErrorMinLength(1)),
                    FormBuilderValidators.maxLength(50,
                        errorText: formErrorMaxLength(50)),
                  ]),
                  keyboardType: TextInputType.text,
                ),
              ),
            ],
          ),
        ),
      )),
    );
  }
}
