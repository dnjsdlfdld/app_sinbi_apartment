import 'package:app_sinbi_apartment/components/component_complain_list.dart';
import 'package:app_sinbi_apartment/components/component_count_title.dart';
import 'package:app_sinbi_apartment/components/component_cupertino.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/components/component_refresh_button.dart';
import 'package:app_sinbi_apartment/components/component_title_design.dart';
import 'package:app_sinbi_apartment/config/config_color.dart';
import 'package:app_sinbi_apartment/model/complain/complain_list_item.dart';
import 'package:app_sinbi_apartment/pages/complain/page_complain_detail.dart';
import 'package:app_sinbi_apartment/pages/complain/page_set_complain.dart';
import 'package:app_sinbi_apartment/pages/complain/page_tab_view_complete.dart';
import 'package:app_sinbi_apartment/pages/complain/page_tab_view_ongoing.dart';
import 'package:app_sinbi_apartment/pages/complain/page_tab_view_received.dart';
import 'package:app_sinbi_apartment/repository/repo_complain.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class PageComplain extends StatefulWidget {
  const PageComplain({super.key});

  @override
  State<PageComplain> createState() => _PageComplainState();
}

class _PageComplainState extends State<PageComplain>
    with SingleTickerProviderStateMixin {
  Icon _searchIcon = Icon(
    Icons.search,
    color: Colors.black,
  );
  bool isSearchClicked = false;
  final TextEditingController _filter = new TextEditingController();
  final GlobalKey<FormBuilderState> _formKey = GlobalKey<FormBuilderState>();
  String _title = '';
  final _scrollController = ScrollController();
  List<ComplainListItem> _list = [];
  late TabController _tabController;
  int _totalItemCount = 0;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 4, vsync: this);
    _loadList();
  }

  Future<void> _loadList({bool reFresh = false}) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    if (reFresh) {
      _list = [];
      _totalItemCount = 0;
    }

    await RepoComplain().getList(title: _title).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _totalItemCount = res.totalItemCount;
        _list = res.list;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '불러오기 실패',
        subTitle: '네트워크가 원활하지 않거나 \n 데이터가 없습니다.',
      ).call();
    });

    if (reFresh) {
      _scrollController.animateTo(0,
          duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
      // 어디에위치? , 1000milliseconds 가 1초 , 모션
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.white,
        child: SafeArea(
          child: DefaultTabController(
            length: 4,
            child: NestedScrollView(
              headerSliverBuilder: (context, innerBoxIsScrolled) {
                return [
                  createSilverAppBar(),
                  // SliverAppBar(
                  //   floating: true,
                  //   snap: false,
                  //   pinned: true,
                  //   backgroundColor: Colors.white,
                  //   expandedHeight: 120.0,
                  //   flexibleSpace: FlexibleSpaceBar(
                  //     title: Row(
                  //       mainAxisAlignment: MainAxisAlignment.start,
                  //       children: [
                  //     // background: Column(children: [
                  //     //   ComponentTitleDesign(title: '민원창구'),
                  //     //   SizedBox(height: 15),
                  //     //   Container(
                  //     //     margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                  //     //     child: Row(
                  //     //       children: [
                  //     //         Icon(Icons.info_outline_rounded,
                  //     //             size: 12, color: Colors.blue),
                  //     //         SizedBox(width: 5),
                  //     //         Text(
                  //     //           '민원창구 게시판 입니다. 아파트 민원만 작성해 주세요.',
                  //     //           style:
                  //     //               TextStyle(color: Colors.grey, fontSize: 12),
                  //     //         ),
                  //     //       ],
                  //     //     ),
                  //     //   ),
                  //     // ]),
                  //     stretchModes: [StretchMode.zoomBackground],
                  //   ),
                  // ),
                  SliverPersistentHeader(
                      pinned: true, delegate: TabBarDelegate()),
                ];
              },
              body: Container(
                margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                child: TabBarView(
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      _buildAllList(),
                      PageTabViewReceived(),
                      PageTabViewOngoing(),
                      PageTabViewComplete(),
                    ]),
              ),
            ),
          ),
        ));
  }

  void _searchPressed() {
    setState(() {
      if (this._searchIcon.icon == Icons.search) {
        this._searchIcon = Icon(
          Icons.close,
          color: Colors.black,
        );
        isSearchClicked = true;
      } else {
        this._searchIcon = Icon(
          Icons.search,
          color: Colors.black,
        );
        isSearchClicked = false;
        _filter.clear();
      }
    });
  }

  SliverAppBar createSilverAppBar() {
    return SliverAppBar(
      title: isSearchClicked
          ? Container(
              padding: EdgeInsets.only(bottom: 0),
              constraints: BoxConstraints(minHeight: 30, maxHeight: 30),
              width: 200,
              child: FormBuilder(
                key: _formKey,
                child: Container(
                  //color: Colors.white,
                  child: FormBuilderTextField(
                    name: 'title',
                    initialValue: _title,
                    keyboardType: TextInputType.text,
                    textAlignVertical: TextAlignVertical.bottom,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: '검색어를 입력하세요...',
                      hintStyle: TextStyle(color: Colors.black),
                      enabledBorder:
                          UnderlineInputBorder(borderSide: BorderSide.none),
                      contentPadding: EdgeInsets.only(bottom: 12),
                      focusedBorder:
                          UnderlineInputBorder(borderSide: BorderSide.none),
                      suffixIcon: IconButton(
                          padding: EdgeInsets.only(top: 5, bottom: 0),
                          onPressed: () {
                            setState(() {
                              _title =
                                  _formKey.currentState!.fields['title']!.value;
                            });
                            _loadList();
                          },
                          icon: Icon(
                            Icons.search,
                            color: Colors.black,
                          )),
                    ),
                  ),
                ),
              ),
            )
          : Text(
              "민원창구",
              style: TextStyle(
                  color: Colors.black,
                  letterSpacing: 2.5,
                  fontWeight: FontWeight.w400),
            ),
      automaticallyImplyLeading: false,
      leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_sharp,
            color: Colors.black,
          )),
      elevation: 0,
      backgroundColor: Colors.white,
      actions: <Widget>[
        IconButton(
            onPressed: () async {
              await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => PageSetComplain()))
                  .then((value) => {
                        setState(() {
                          _loadList();
                        })
                      });
            },
            icon: Icon(
              Icons.post_add,
              color: Colors.black,
            )),
        RawMaterialButton(
          elevation: 0.0,
          onPressed: () {
            _searchPressed();
          },
          constraints: BoxConstraints.tightFor(
            width: 56,
            height: 56,
          ),
          shape: CircleBorder(),
          child: _searchIcon,
        ),
      ],
      expandedHeight: 250,
      floating: false,
      pinned: true,
      snap: false,
      flexibleSpace: FlexibleSpaceBar(
          titlePadding: EdgeInsets.only(bottom: 15),
          centerTitle: true,
          background: Image.asset(
            "assets/xi2.jpg",
            fit: BoxFit.cover,
          )),
    );
  }

  Widget _buildAllList() {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            child: ComponentCountTitle(
                icon: Icons.account_box,
                count: _totalItemCount,
                unitName: '건',
                itemName: '민원'),
          ),
          ListView.builder(
              controller: _scrollController,
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _list.length,
              itemBuilder: (_, index) => ComponentComplainList(
                  complainListItem: _list[index],
                  callback: () async {
                    await Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PageComplainDetail(
                                complainId: _list[index].complainId)));
                  })),
        ],
      ),
    );
  }
}

// class SampleHeaderDelegate extends SliverPersistentHeaderDelegate {
//   SampleHeaderDelegate({required this.widget});
//
//   Widget widget;
//
//   @override
//   Widget build(
//       BuildContext context, double shrinkOffset, bool overlapsContent) {
//     return widget;
//   }
//
//   @override
//   double get maxExtent => 50;
//
//   @override
//   double get minExtent => 50;
//
//   @override
//   bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
//     return false;
//   }
// }
class TabBarDelegate extends SliverPersistentHeaderDelegate {
  const TabBarDelegate();

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      color: Colors.white,
      child: TabBar(
        tabs: [
          Tab(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              color: Colors.white,
              child: const Text(
                "전체",
              ),
            ),
          ),
          Tab(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              color: Colors.white,
              child: const Text(
                "접수",
              ),
            ),
          ),
          Tab(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              color: Colors.white,
              child: const Text(
                "진행",
              ),
            ),
          ),
          Tab(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              color: Colors.white,
              child: const Text(
                "완료",
              ),
            ),
          ),
        ],
        indicatorWeight: 2,
        //padding: const EdgeInsets.symmetric(horizontal: 16.0),
        unselectedLabelColor: Colors.grey,
        labelColor: Colors.black,
        indicatorColor: Colors.black,
        indicatorSize: TabBarIndicatorSize.label,
      ),
    );
  }

  @override
  double get maxExtent => 48;

  @override
  double get minExtent => 48;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}
