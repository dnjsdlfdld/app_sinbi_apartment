import 'package:app_sinbi_apartment/components/component_cupertino.dart';
import 'package:app_sinbi_apartment/components/component_complain_detail_comments.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/config/config_form_validator.dart';
import 'package:app_sinbi_apartment/model/complain/complain_comments_item.dart';
import 'package:app_sinbi_apartment/model/complain/complain_comments_request.dart';
import 'package:app_sinbi_apartment/model/complain/complain_detail_item.dart';
import 'package:app_sinbi_apartment/repository/repo_complain.dart';
import 'package:app_sinbi_apartment/repository/repo_complain_comments.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';

class PageComplainDetail extends StatefulWidget {
  const PageComplainDetail({super.key, required this.complainId});

  final int complainId;

  @override
  State<PageComplainDetail> createState() => _PageComplainDetailState();
}

class _PageComplainDetailState extends State<PageComplainDetail> {
  ComplainDetailItem _item = ComplainDetailItem(0, '', '', '', DateTime.now());
  final _formKey = GlobalKey<FormBuilderState>();
  List<ComplainCommentsItem> _listComments = [];
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  int _totalItemCount = 0;

  final DateFormat dateFormat = DateFormat('yyyy-MM-dd HH:mm:ss');

  @override
  void initState() {
    super.initState();
    _loadDetail();
    _loadComments();
  }

  Future<void> _setComments(ComplainCommentsRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoComplainComments()
        .setComments(widget.complainId, request)
        .then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '댓글이 등록되었습니다.',
        subTitle: res.msg, // 백엔드에서 반환받은 메세지 넣기
      ).call();
      Navigator.pop(context);
      setState(() {
        _loadComments();
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '등록 실패',
        subTitle: '댓글 등록에 실패하였습니다.',
      ).call();
    });
  }

  Future<void> _loadComments() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoComplainComments().getComments(widget.complainId).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _listComments = res.list;

        _totalItemCount = res.totalItemCount;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '불러오기 실패',
        subTitle: '네트워크가 원활하지 않거나 \n 데이터가 없습니다.',
      ).call();
    });
  }

  Future<void> _loadDetail() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoComplain().getDetail(widget.complainId).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _item = res.data;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '불러오기 실패',
        subTitle: '네트워크가 원활하지 않거나 \n 데이터가 없습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: this._scaffoldKey,
      appBar: AppBar(
        title: Text(
          '민원창구',
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        automaticallyImplyLeading: true,
        iconTheme: IconThemeData(color: Colors.black),
        elevation: 0,
      ),
      body: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(20, 30, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('${_item.title}',
                        style: TextStyle(
                            fontSize: 20,
                            letterSpacing: 1.0,
                            fontWeight: FontWeight.w600)),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20, 5, 0, 0),
                child: Row(
                  children: [
                    Text('${_item.residentFullName}',
                        style: TextStyle(color: Colors.grey))
                  ],
                ),
              ),
              SizedBox(height: 20),
              Container(
                margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(dateFormat.format(_item.dateCreate),
                        style: TextStyle(color: Colors.grey, fontSize: 12))
                  ],
                ),
              ),
              SizedBox(height: 50),
              Container(
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Row(
                  children: [
                    Container(
                      width: 300,
                      child: Text('${_item.content}'),
                    )
                  ],
                ),
              ),
              SizedBox(height: 100),
              Container(
                width: 410,
                height: 35,
                decoration: BoxDecoration(
                    border: Border(
                        top: BorderSide(color: Colors.grey),
                        bottom: BorderSide(color: Colors.grey))),
                child: Row(
                  children: [
                    const Text('    댓글 '),
                    Text('${_totalItemCount}개',
                        style: TextStyle(color: Colors.green)),
                    SizedBox(width: 270),
                    TextButton(
                      onPressed: () => this
                          ._scaffoldKey
                          .currentState
                          ?.showBottomSheet((ctx) => _buildBottomSheet(ctx)),
                      child: const Text('댓글쓰기'),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 5),
              _buildBody(),
            ],
          ),
        ),
      ),
      backgroundColor: Colors.white,
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Column(
        children: [
          // for (int i = 0; i < _listComments.length; i++)
          //   Text('${_listComments[i].content}')
          ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _listComments.length,
              itemBuilder: (_, index) => ComponentComplainDetailComments(
                    commentsItem: _listComments[index],
                  )),
        ],
      ),
    );
  }

  Container _buildBottomSheet(BuildContext context) {
    return Container(
        height: 60,
        padding: const EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.white, width: 2.0),
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Row(
              children: [
                Container(
                  width: 300,
                  height: 40,
                  child: FormBuilderTextField(
                    minLines: 1,
                    maxLines: 2,
                    name: 'content',
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(horizontal: 20),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                          borderSide: BorderSide(color: Colors.grey)),
                      //icon: Icon(Icons.telegram),
                      hintText: '댓글을 입력하세요...',
                    ),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(1,
                          errorText: formErrorMinLength(1)),
                      FormBuilderValidators.maxLength(50,
                          errorText: formErrorMaxLength(50)),
                    ]),
                  ),
                ),
                SizedBox(width: 30),
                Container(
                  width: 60,
                  alignment: Alignment.center,
                  child: IconButton(
                    icon: Icon(Icons.send_rounded),
                    style: ElevatedButton.styleFrom(minimumSize: Size(0, 40)),
                    onPressed: () {
                      if (_formKey.currentState?.saveAndValidate() ?? false) {
                        ComplainCommentsRequest request =
                            ComplainCommentsRequest(
                          _formKey.currentState!.fields['content']!.value,
                        );

                        _setComments(request);
                      }
                    },
                  ),
                )
              ],
            )));
  }
}
