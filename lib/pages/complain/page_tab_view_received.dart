import 'package:app_sinbi_apartment/components/component_complain_list.dart';
import 'package:app_sinbi_apartment/components/component_count_title.dart';
import 'package:app_sinbi_apartment/components/component_cupertino.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/components/component_refresh_button.dart';
import 'package:app_sinbi_apartment/model/complain/complain_list_item.dart';
import 'package:app_sinbi_apartment/pages/complain/page_complain_detail.dart';
import 'package:app_sinbi_apartment/repository/repo_complain.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageTabViewReceived extends StatefulWidget {
  const PageTabViewReceived({Key? key}) : super(key: key);

  @override
  State<PageTabViewReceived> createState() => _PageTabViewReceivedState();
}

class _PageTabViewReceivedState extends State<PageTabViewReceived> {
  List<ComplainListItem> _list = [];
  int _totalItemCount = 0;

  @override
  void initState() {
    super.initState();
    _loadReceived();
  }

  Future<void> _loadReceived() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCupertino(cancelFunc: cancelFunc);
    });

    await RepoComplain().getReceived().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list;
        _totalItemCount = res.totalItemCount;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '불러오기 실패',
        subTitle: '네트워크가 원활하지 않거나 \n 데이터가 없습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        children: [
          Container(
            child: ComponentCountTitle(
                icon: Icons.account_box,
                count: _totalItemCount,
                unitName: '건',
                itemName: '민원'),
          ),
          ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _list.length,
              itemBuilder: (_, index) => ComponentComplainList(
                  complainListItem: _list[index],
                  callback: () async {
                    await Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PageComplainDetail(
                                complainId: _list[index].complainId)));
                  }))
        ],
      ),
    );
  }
}
