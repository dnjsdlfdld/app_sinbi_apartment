import 'package:app_sinbi_apartment/config/config_api.dart';
import 'package:app_sinbi_apartment/functions/token_lib.dart';
import 'package:app_sinbi_apartment/model/news/news_comments_list_result.dart';
import 'package:app_sinbi_apartment/model/news/news_comments_request.dart';
import 'package:app_sinbi_apartment/model/news/news_request_common_result.dart';
import 'package:dio/dio.dart';

class RepoNewsComments {
  Future<NewsCommentsListResult> getComments(int newsId) async {
    const String baseUrl = '$apiUri/news-comments/all/{newsId}';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(
        baseUrl.replaceAll('{newsId}', newsId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return NewsCommentsListResult.fromJson(response.data);
  }

  Future<NewsRequestCommonResult> setComments(
      int newsId, NewsCommentsRequest request) async {
    const String baseUrl = '$apiUri/news-comments/resident/news/{newsId}';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.post(
        baseUrl.replaceAll('{newsId}', newsId.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return NewsRequestCommonResult.fromJson(response.data);
  }
}
