import 'package:app_sinbi_apartment/config/config_api.dart';
import 'package:app_sinbi_apartment/functions/token_lib.dart';
import 'package:app_sinbi_apartment/model/main_home/resident_name_single_result.dart';
import 'package:dio/dio.dart';

class RepoMainHome {
  Future<ResidentNameSingleResult> getName() async {
    const String baseUrl = '$apiUri/resident/name';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return ResidentNameSingleResult.fromJson(response.data);
  }
}
