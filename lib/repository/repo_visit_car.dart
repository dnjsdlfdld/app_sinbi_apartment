import 'package:app_sinbi_apartment/config/config_api.dart';
import 'package:app_sinbi_apartment/functions/token_lib.dart';
import 'package:app_sinbi_apartment/model/visitcar/visit_car_common_result.dart';
import 'package:app_sinbi_apartment/model/visitcar/visit_car_delete_result.dart';
import 'package:app_sinbi_apartment/model/visitcar/visit_car_list_result.dart';
import 'package:app_sinbi_apartment/model/visitcar/visit_car_request.dart';
import 'package:dio/dio.dart';

class RepoVisitCar {
  Future<VisitCarCommonResult> setVisitCar(VisitCarRequest request) async {
    const String baseUrl = '$apiUri/visit-car/new';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.post(baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return VisitCarCommonResult.fromJson(response.data);
  }

  Future<VisitCarListResult> getVisitCar() async {
    const String baseUrl = '$apiUri/visit-car/all';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return VisitCarListResult.fromJson(response.data);
  }

  Future<VisitCarDeleteResult> delVisitCar(int visitCarId) async {
    const String baseUrl = '$apiUri/visit-car/visit-car-id/{visitCarId}';
    //String? token = await TokenLib.getToken();

    Dio dio = Dio();

    final response = await dio.delete(
        baseUrl.replaceAll('{visitCarId}', visitCarId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return VisitCarDeleteResult.fromJson(response.data);
  }
}
