import 'package:app_sinbi_apartment/config/config_api.dart';
import 'package:app_sinbi_apartment/model/login/login_single_result.dart';
import 'package:app_sinbi_apartment/model/login/login_request.dart';
import 'package:dio/dio.dart';

class RepoLogin {
  Future<LoginSingleResult> doLogin(LoginRequest request) async {
    const String baseUrl = '$apiUri/login/app/user';

    Dio dio = Dio();
    final response = await dio.post(
      baseUrl,
      data: request.toJson(),
      options: Options(
        followRedirects: false,
        validateStatus: (status) => (status == 200),
      ),
    );

    return LoginSingleResult.fromJson(response.data);
  }
}
