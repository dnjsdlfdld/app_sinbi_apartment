import 'package:app_sinbi_apartment/config/config_api.dart';
import 'package:app_sinbi_apartment/functions/token_lib.dart';
import 'package:app_sinbi_apartment/model/meeting/meeting_comments_list_result.dart';
import 'package:app_sinbi_apartment/model/meeting/meeting_comments_request.dart';
import 'package:app_sinbi_apartment/model/meeting/meeting_common_result.dart';
import 'package:dio/dio.dart';

class RepoMeetingComments {
  Future<MeetingCommentsListResult> getComments(int meetingId) async {
    const String baseUrl = '$apiUri/meeting-comments/comments/{meetingId}';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(
        baseUrl.replaceAll('{meetingId}', meetingId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return MeetingCommentsListResult.fromJson(response.data);
  }

  Future<MeetingCommonResult> setComments(
      int meetingId, MeetingCommentsRequest request) async {
    const String baseUrl =
        '$apiUri/meeting-comments/resident/meeting/{meetingId}';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.post(
        baseUrl.replaceAll('{meetingId}', meetingId.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return MeetingCommonResult.fromJson(response.data);
  }
}