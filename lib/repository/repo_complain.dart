import 'package:app_sinbi_apartment/config/config_api.dart';
import 'package:app_sinbi_apartment/functions/token_lib.dart';
import 'package:app_sinbi_apartment/model/complain/complain_detail_common_result.dart';
import 'package:app_sinbi_apartment/model/complain/complain_list_result.dart';
import 'package:app_sinbi_apartment/model/complain/complain_request.dart';
import 'package:app_sinbi_apartment/model/complain/complain_request_common_result.dart';
import 'package:dio/dio.dart';

class RepoComplain {
  Future<ComplainRequestCommonResult> setData(ComplainRequest request) async {
    const String baseUrl = '$apiUri/complain/new';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.post(baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return ComplainRequestCommonResult.fromJson(response.data);
  }

  Future<ComplainListResult> getList({String title = '테'}) async {
    const String baseUrl = '$apiUri/complain/all';
    Map<String, dynamic> params = {};
    if (title != '') params['title'] = Uri.encodeFull(title);

    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(baseUrl,
        queryParameters: params,
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return ComplainListResult.fromJson(response.data);
  }

  Future<ComplainListResult> getReceived() async {
    const String baseUrl = '$apiUri/complain/received';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return ComplainListResult.fromJson(response.data);
  }

  Future<ComplainListResult> getOngoing() async {
    const String baseUrl = '$apiUri/complain/ongoing';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return ComplainListResult.fromJson(response.data);
  }

  Future<ComplainListResult> getComplete() async {
    const String baseUrl = '$apiUri/complain/complete';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return ComplainListResult.fromJson(response.data);
  }

  Future<ComplainDetailCommonResult> getDetail(int complainId) async {
    const String baseUrl = '$apiUri/complain/detail/{complainId}';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(
        baseUrl.replaceAll('{complainId}', complainId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return ComplainDetailCommonResult.fromJson(response.data);
  }
}
