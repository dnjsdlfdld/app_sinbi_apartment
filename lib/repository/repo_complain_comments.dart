import 'package:app_sinbi_apartment/config/config_api.dart';
import 'package:app_sinbi_apartment/functions/token_lib.dart';
import 'package:app_sinbi_apartment/model/complain/complain_comments_list_result.dart';
import 'package:app_sinbi_apartment/model/complain/complain_comments_request.dart';
import 'package:app_sinbi_apartment/model/complain/complain_request_common_result.dart';
import 'package:dio/dio.dart';

class RepoComplainComments {
  Future<ComplainCommentsListResult> getComments(int complainId) async {
    const String baseUrl = '$apiUri/complain-comments/comments/{complainId}';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(
        baseUrl.replaceAll('{complainId}', complainId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return ComplainCommentsListResult.fromJson(response.data);
  }

  Future<ComplainRequestCommonResult> setComments(
      int complainId, ComplainCommentsRequest request) async {
    const String baseUrl =
        '$apiUri/complain-comments/resident/complain/{complainId}';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.post(
        baseUrl.replaceAll('{complainId}', complainId.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return ComplainRequestCommonResult.fromJson(response.data);
  }
}
