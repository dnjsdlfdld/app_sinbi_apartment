import 'package:app_sinbi_apartment/config/config_api.dart';
import 'package:app_sinbi_apartment/functions/token_lib.dart';
import 'package:app_sinbi_apartment/model/news/news_detail_common_result.dart';
import 'package:app_sinbi_apartment/model/news/news_list_result.dart';
import 'package:dio/dio.dart';

class RepoNews {
  Future<NewsListResult> getList() async {
    const String baseUrl = '$apiUri/news/all';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return NewsListResult.fromJson(response.data);
  }

  Future<NewsDetailCommonResult> getDetail(int newsId) async {
    const String baseUrl = '$apiUri/news/detail/{newsId}';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(
        baseUrl.replaceAll('{newsId}', newsId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return NewsDetailCommonResult.fromJson(response.data);
  }
}
