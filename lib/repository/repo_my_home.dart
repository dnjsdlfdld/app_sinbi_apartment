import 'package:app_sinbi_apartment/config/config_api.dart';
import 'package:app_sinbi_apartment/functions/token_lib.dart';
import 'package:app_sinbi_apartment/model/myhome/admin_cost_single_result.dart';
import 'package:app_sinbi_apartment/model/myhome/house_member_list_result.dart';
import 'package:app_sinbi_apartment/model/myhome/my_car_list_result.dart';
import 'package:app_sinbi_apartment/model/myhome/my_info_item_common_result.dart';
import 'package:dio/dio.dart';

class RepoMyHome {
  Future<MyInfoItemCommonResult> getMyInfo() async {
    const String baseUrl = '$apiUri/resident/my-info';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return MyInfoItemCommonResult.fromJson(response.data);
  }

  Future<HouseMemberListResult> getHouseMember() async {
    const String baseUrl = '$apiUri/house-member/family-detail';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return HouseMemberListResult.fromJson(response.data);
  }

  Future<MyCarListResult> getCarList() async {
    const String baseUrl = '$apiUri/my-car/resident/car-all';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return MyCarListResult.fromJson(response.data);
  }

  Future<AdminCostSingleResult> getAdminCost() async {
    const String baseUrl = '$apiUri/admin-cost/cost/month';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return AdminCostSingleResult.fromJson(response.data);
  }
}
