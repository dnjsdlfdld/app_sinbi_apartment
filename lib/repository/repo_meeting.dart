import 'package:app_sinbi_apartment/config/config_api.dart';
import 'package:app_sinbi_apartment/functions/token_lib.dart';
import 'package:app_sinbi_apartment/model/meeting/meeting_category_list_result.dart';
import 'package:app_sinbi_apartment/model/meeting/meeting_detail_common_result.dart';
import 'package:app_sinbi_apartment/model/meeting/meeting_list_result.dart';
import 'package:dio/dio.dart';

class RepoMeeting {
  Future<MeetingCategoryListResult> getMeetingCategory() async {
    const String baseUrl = '$apiUri/meeting/category';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return MeetingCategoryListResult.fromJson(response.data);
  }

  Future<MeetingListResult> getMeetingList(String category) async {
    const String baseUrl = '$apiUri/meeting/category/name';
    String? token = await TokenLib.getToken();
    Map<String, dynamic> params = {};
    params['category'] = Uri.encodeFull(category);

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(baseUrl,
        queryParameters: params,
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    print(response);
    return MeetingListResult.fromJson(response.data);
  }

  Future<MeetingDetailCommonResult> getMeetingDetail(int categoryId) async {
    const String baseUrl = '$apiUri/meeting/category/{categoryId}';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(baseUrl.replaceAll('{categoryId}', categoryId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)));
    return MeetingDetailCommonResult.fromJson(response.data);
  }
}
