class ResidentNameItem {
  String name;

  ResidentNameItem(
      this.name
      );

  factory ResidentNameItem.fromJson(Map<String, dynamic> json) {
    return ResidentNameItem(
      json['name']
    );
  }
}