import 'package:app_sinbi_apartment/model/main_home/resident_name_item.dart';

class ResidentNameSingleResult {
  bool isSuccess;
  int code;
  String msg;
  ResidentNameItem data;

  ResidentNameSingleResult(
      this.isSuccess,
      this.code,
      this.msg,
      this.data
      );

  factory ResidentNameSingleResult.fromJson(Map<String, dynamic> json) {
    return ResidentNameSingleResult(
        json['isSuccess'],
        json['code'],
        json['msg'],
        ResidentNameItem.fromJson(json['data'])
    );
  }
}