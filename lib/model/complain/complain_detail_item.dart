class ComplainDetailItem {
  int complainId;
  String residentFullName;
  String title;
  String content;
  DateTime dateCreate;

  ComplainDetailItem(
      this.complainId,
      this.residentFullName,
      this.title,
      this.content,
      this.dateCreate
      );

  factory ComplainDetailItem.fromJson(Map<String, dynamic> json) {
    return ComplainDetailItem(
      json['complainId'],
      json['residentFullName'],
      json['title'],
      json['content'],
      DateTime.parse(json['dateCreate']),
    );
  }
}