class ComplainListItem {
  int complainId;
  String title;
  String serviceState;

  ComplainListItem(
      this.complainId,
      this.title,
      this.serviceState
      );

  factory ComplainListItem.fromJson(Map<String, dynamic> json) {
    return ComplainListItem(
      json['complainId'],
      json['title'],
      json['serviceState']
    );
  }
}