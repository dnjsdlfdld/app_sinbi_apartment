class ComplainRequest {
  String title;
  String content;

  ComplainRequest(
      this.title,
      this.content
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['title'] = this.title;
    data['content'] = this.content;
    return data;
  }
}