class ComplainCommentsItem {
  String residentFullName;
  String content;

  ComplainCommentsItem(
      this.residentFullName,
      this.content
      );

  factory ComplainCommentsItem.fromJson(Map<String, dynamic> json) {
    return ComplainCommentsItem(
      json['residentFullName'],
      json['content']
    );
  }
}