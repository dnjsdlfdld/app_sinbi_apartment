import 'package:app_sinbi_apartment/model/complain/complain_comments_item.dart';

class ComplainCommentsListResult {
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<ComplainCommentsItem> list;

  ComplainCommentsListResult(
      this.isSuccess,
      this.code,
      this.msg,
      this.totalItemCount,
      this.totalPage,
      this.currentPage,
      this.list
      );

  factory ComplainCommentsListResult.fromJson(Map<String, dynamic> json) {
    return ComplainCommentsListResult(
        json['isSuccess'],
        json['code'],
        json['msg'],
        json['totalItemCount'],
        json['totalPage'],
        json['currentPage'],
        json['list'] == null ? [] : (json['list'] as List).map((e) => ComplainCommentsItem.fromJson(e)).toList()
    );
  }
}