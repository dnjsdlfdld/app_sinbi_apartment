import 'package:app_sinbi_apartment/model/complain/complain_detail_item.dart';

class ComplainDetailCommonResult {
  bool isSuccess;
  int code;
  String msg;
  ComplainDetailItem data;

  ComplainDetailCommonResult(
      this.isSuccess,
      this.code,
      this.msg,
      this.data
      );

  factory ComplainDetailCommonResult.fromJson(Map<String, dynamic> json) {
    return ComplainDetailCommonResult(
        json['isSuccess'],
        json['code'],
        json['msg'],
        ComplainDetailItem.fromJson(json['data'])
    );
  }
}