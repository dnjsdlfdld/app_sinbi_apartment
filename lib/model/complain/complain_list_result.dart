import 'package:app_sinbi_apartment/model/complain/complain_list_item.dart';

class ComplainListResult {
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<ComplainListItem> list;

  ComplainListResult(
      this.isSuccess,
      this.code,
      this.msg,
      this.totalItemCount,
      this.totalPage,
      this.currentPage,
      this.list
      );

  factory ComplainListResult.fromJson(Map<String, dynamic> json) {
    return ComplainListResult(
      json['isSuccess'],
      json['code'],
      json['msg'],
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      json['list'] == null ? [] : (json['list'] as List).map((e) => ComplainListItem.fromJson(e)).toList()
    );
  }
}