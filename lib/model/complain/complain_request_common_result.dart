class ComplainRequestCommonResult {
  bool isSuccess;
  int code;
  String msg;

  ComplainRequestCommonResult(
      this.isSuccess,
      this.code,
      this.msg
      );

  factory ComplainRequestCommonResult.fromJson(Map<String, dynamic> json) {
    return ComplainRequestCommonResult(
      json['isSuccess'],
      json['code'],
      json['msg']
    );
  }
}