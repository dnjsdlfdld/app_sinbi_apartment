import 'package:app_sinbi_apartment/model/visitcar/visit_car_request.dart';

class VisitCarCommonResult {
  bool isSuccess;
  int code;
  String msg;

  VisitCarCommonResult(
      this.isSuccess,
      this.code,
      this.msg,
      );

  factory VisitCarCommonResult.fromJson(Map<String, dynamic> json) {
    return VisitCarCommonResult(
      json['isSuccess'],
      json['code'],
      json['msg'],
    );
  }
}