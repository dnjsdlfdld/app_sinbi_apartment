class VisitCarListItem {
  int visitCarId;
  String carNum;
  String approvalState;
  DateTime dateCreate;

  VisitCarListItem(
      this.visitCarId,
      this.carNum,
      this.approvalState,
      this.dateCreate
      );

  factory VisitCarListItem.fromJson(Map<String, dynamic> json) {
    return VisitCarListItem(
      json['visitCarId'],
      json['carNum'],
      json['approvalState'],
      DateTime.parse(json['dateCreate'])
    );
  }
}