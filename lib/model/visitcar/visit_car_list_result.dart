import 'package:app_sinbi_apartment/model/visitcar/visit_car_list_item.dart';

class VisitCarListResult {
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<VisitCarListItem> list;

  VisitCarListResult(
      this.isSuccess,
      this.code,
      this.msg,
      this.totalItemCount,
      this.totalPage,
      this.currentPage,
      this.list
      );

  factory VisitCarListResult.fromJson(Map<String, dynamic> json) {
    return VisitCarListResult(
        json['isSuccess'],
        json['code'],
        json['msg'],
        json['totalItemCount'],
        json['totalPage'],
        json['currentPage'],
        json['list'] == null ? [] : (json['list'] as List).map((e) => VisitCarListItem.fromJson(e)).toList()
    );
  }
}