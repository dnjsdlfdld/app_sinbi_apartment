class VisitCarDeleteResult {
  bool isSuccess;
  int code;
  String msg;

  VisitCarDeleteResult(
      this.isSuccess,
      this.code,
      this.msg
      );

  factory VisitCarDeleteResult.fromJson(Map<String, dynamic> json) {
    return VisitCarDeleteResult(
      json['isSuccess'],
      json['code'],
      json['msg']
    );
  }
}