class VisitCarRequest {
  String carNum;
  String dateStart;
  String dateEnd;

  VisitCarRequest(
      this.carNum,
      this.dateStart,
      this.dateEnd
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['carNum'] = this.carNum;
    data['dateStart'] = this.dateStart;
    data['dateEnd'] = this.dateEnd;

    return data;
  }
}