import 'package:app_sinbi_apartment/model/news/news_detail_item.dart';

class NewsDetailCommonResult {
  bool isSuccess;
  int code;
  String msg;
  NewsDetailItem data;

  NewsDetailCommonResult(
      this.isSuccess,
      this.code,
      this.msg,
      this.data
      );

  factory NewsDetailCommonResult.fromJson(Map<String, dynamic> json) {
    return NewsDetailCommonResult(
        json['isSuccess'],
        json['code'],
        json['msg'],
        NewsDetailItem.fromJson(json['data'])
    );
  }
}