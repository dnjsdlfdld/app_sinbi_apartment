import 'package:app_sinbi_apartment/model/news/news_list.dart';

class NewsListResult {
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<NewsList> list;

  NewsListResult(
      this.isSuccess,
      this.code,
      this.msg,
      this.totalItemCount,
      this.totalPage,
      this.currentPage,
      this.list
      );

  factory NewsListResult.fromJson(Map<String, dynamic> json) {
    return NewsListResult(
        json['isSuccess'],
        json['code'],
        json['msg'],
        json['totalItemCount'],
        json['totalPage'],
        json['currentPage'],
        json['list'] == null ? [] : (json['list'] as List).map((e) => NewsList.fromJson(e)).toList()
    );
  }
}