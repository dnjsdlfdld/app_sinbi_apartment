import 'package:app_sinbi_apartment/model/news/news_comments_item.dart';

class NewsCommentsListResult {
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<NewsCommentsItem> list;

  NewsCommentsListResult(
      this.isSuccess,
      this.code,
      this.msg,
      this.totalItemCount,
      this.totalPage,
      this.currentPage,
      this.list
      );

  factory NewsCommentsListResult.fromJson(Map<String, dynamic> json) {
    return NewsCommentsListResult(
        json['isSuccess'],
        json['code'],
        json['msg'],
        json['totalItemCount'],
        json['totalPage'],
        json['currentPage'],
        json['list'] == null ? [] : (json['list'] as List).map((e) => NewsCommentsItem.fromJson(e)).toList()
    );
  }
}