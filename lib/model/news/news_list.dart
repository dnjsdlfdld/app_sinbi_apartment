class NewsList {
  int newsId;
  String title;
  String newsState;

  NewsList(
      this.newsId,
      this.title,
      this.newsState
      );

  factory NewsList.fromJson(Map<String, dynamic> json) {
    return NewsList(
      json['newsId'],
      json['title'],
      json['newsState']
    );
  }
}