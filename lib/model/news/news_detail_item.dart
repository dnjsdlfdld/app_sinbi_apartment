class NewsDetailItem {
  int newsId;
  String manageName;
  String title;
  String content;
  DateTime dateCreate;

  NewsDetailItem(
      this.newsId,
      this.manageName,
      this.title,
      this.content,
      this.dateCreate
      );

  factory NewsDetailItem.fromJson(Map<String, dynamic> json) {
    return NewsDetailItem(
      json['newsId'],
      json['manageName'],
      json['title'],
      json['content'],
      DateTime.parse(json['dateCreate'])
    );
  }
}