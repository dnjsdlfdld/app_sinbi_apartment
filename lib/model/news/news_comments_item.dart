class NewsCommentsItem {
  String residentFullName;
  String content;

  NewsCommentsItem(
      this.residentFullName,
      this.content
      );

  factory NewsCommentsItem.fromJson(Map<String, dynamic> json) {
    return NewsCommentsItem(
        json['residentFullName'],
        json['content']
    );
  }
}