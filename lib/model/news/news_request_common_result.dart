class NewsRequestCommonResult {
  bool isSuccess;
  int code;
  String msg;

  NewsRequestCommonResult(
      this.isSuccess,
      this.code,
      this.msg
      );

  factory NewsRequestCommonResult.fromJson(Map<String, dynamic> json) {
    return NewsRequestCommonResult(
      json['isSuccess'],
      json['code'],
      json['msg']
    );
  }
}