import 'package:app_sinbi_apartment/model/meeting/meeting_category_item.dart';

class MeetingCategoryListResult {
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<MeetingCategoryItem> list;

  MeetingCategoryListResult(
      this.isSuccess,
      this.code,
      this.msg,
      this.totalItemCount,
      this.totalPage,
      this.currentPage,
      this.list
      );

  factory MeetingCategoryListResult.fromJson(Map<String, dynamic> json) {
    return MeetingCategoryListResult(
        json['isSuccess'],
        json['code'],
        json['msg'],
        json['totalItemCount'],
        json['totalPage'],
        json['currentPage'],
        json['list'] == null ? [] : (json['list'] as List).map((e) => MeetingCategoryItem.fromJson(e)).toList()
    );
  }
}