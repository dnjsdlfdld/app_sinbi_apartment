import 'package:app_sinbi_apartment/model/meeting/meeting_list_item.dart';

class MeetingListResult {
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<MeetingListItem> list;

  MeetingListResult(
      this.isSuccess,
      this.code,
      this.msg,
      this.totalItemCount,
      this.totalPage,
      this.currentPage,
      this.list
      );

  factory MeetingListResult.fromJson(Map<String, dynamic> json) {
    return MeetingListResult(
        json['isSuccess'],
        json['code'],
        json['msg'],
        json['totalItemCount'],
        json['totalPage'],
        json['currentPage'],
        json['list'] == null ? [] : (json['list'] as List).map((e) => MeetingListItem.fromJson(e)).toList()
    );
  }
}