class MeetingCommentsListItem {
  String residentFullName;
  String content;

  MeetingCommentsListItem(
      this.residentFullName,
      this.content
      );

  factory MeetingCommentsListItem.fromJson(Map<String, dynamic> json) {
    return MeetingCommentsListItem(
        json['residentFullName'],
        json['content']
    );
  }
}