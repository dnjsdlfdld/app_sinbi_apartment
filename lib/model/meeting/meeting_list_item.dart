class MeetingListItem {
  int id;
  String title;

  MeetingListItem(
      this.id,
      this.title
      );

  factory MeetingListItem.fromJson(Map<String, dynamic> json) {
    return MeetingListItem(
      json['id'],
      json['title']
    );
  }
}