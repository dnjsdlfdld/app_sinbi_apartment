import 'package:app_sinbi_apartment/model/meeting/meeting_detail_item.dart';

class MeetingDetailCommonResult {
  bool isSuccess;
  int code;
  String msg;
  MeetingDetailItem data;

  MeetingDetailCommonResult(
      this.isSuccess,
      this.code,
      this.msg,
      this.data
      );

  factory MeetingDetailCommonResult.fromJson(Map<String, dynamic> json) {
    return MeetingDetailCommonResult(
        json['isSuccess'],
        json['code'],
        json['msg'],
        MeetingDetailItem.fromJson(json['data'])
    );
  }
}