class MeetingDetailItem {
  int meetingId;
  String residentFullName;
  String title;
  String content;
  DateTime dateCreate;

  MeetingDetailItem(
      this.meetingId,
      this.residentFullName,
      this.title,
      this.content,
      this.dateCreate
      );

  factory MeetingDetailItem.fromJson(Map<String, dynamic> json) {
    return MeetingDetailItem(
      json['meetingId'],
      json['residentFullName'],
      json['title'],
      json['content'],
      DateTime.parse(json['dateCreate'])
    );
  }
}