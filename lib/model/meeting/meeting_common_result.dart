class MeetingCommonResult {
  bool isSuccess;
  int code;
  String msg;

  MeetingCommonResult(
      this.isSuccess,
      this.code,
      this.msg
      );

  factory MeetingCommonResult.fromJson(Map<String, dynamic> json) {
    return MeetingCommonResult(
        json['isSuccess'],
        json['code'],
        json['msg']
    );
  }
}