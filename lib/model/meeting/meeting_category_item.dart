class MeetingCategoryItem {
  String category;
  String categoryName;
  String categoryIntroduce;
  String imgUrl;

  MeetingCategoryItem(
      this.category,
      this.categoryName,
      this.categoryIntroduce,
      this.imgUrl
      );

  factory MeetingCategoryItem.fromJson(Map<String, dynamic> json) {
    return MeetingCategoryItem(
      json['category'],
      json['categoryName'],
      json['categoryIntroduce'],
      json['imgUrl']
    );
  }
}