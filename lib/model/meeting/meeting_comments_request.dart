class MeetingCommentsRequest {
  String content;

  MeetingCommentsRequest(
      this.content
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['content'] = this.content;
    return data;
  }
}