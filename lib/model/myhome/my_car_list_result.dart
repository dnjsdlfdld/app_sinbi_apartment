import 'package:app_sinbi_apartment/model/myhome/my_car_list.dart';

class MyCarListResult {
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<MyCarList> list;

  MyCarListResult(
      this.isSuccess,
      this.code,
      this.msg,
      this.totalItemCount,
      this.totalPage,
      this.currentPage,
      this.list
      );

  factory MyCarListResult.fromJson(Map<String, dynamic> json) {
    return MyCarListResult(
        json['isSuccess'],
        json['code'],
        json['msg'],
        json['totalItemCount'],
        json['totalPage'],
        json['currentPage'],
        json['list'] == null ? [] : (json['list'] as List).map((e) => MyCarList.fromJson(e)).toList()
    );
  }
}