class MyInfoItem {
  String residentName;
  String phone;
  String addressFullName;

  MyInfoItem(
      this.residentName,
      this.phone,
      this.addressFullName
      );

  factory MyInfoItem.fromJson(Map<String, dynamic> json) {
    return MyInfoItem(
      json['residentName'],
      json['phone'],
      json['addressFullName']
    );
  }
}