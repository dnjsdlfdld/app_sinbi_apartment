class HouseMemberNameList {
  String houseMemberName;

  HouseMemberNameList(
      this.houseMemberName
      );

  factory HouseMemberNameList.fromJson(Map<String, dynamic> json) {
    return HouseMemberNameList(
      json['houseMemberName']
    );
  }
}