import 'package:app_sinbi_apartment/model/complain/complain_list_item.dart';
import 'package:app_sinbi_apartment/model/myhome/house_member_name_list.dart';

class HouseMemberListResult {
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<HouseMemberNameList> list;

  HouseMemberListResult(
      this.isSuccess,
      this.code,
      this.msg,
      this.totalItemCount,
      this.totalPage,
      this.currentPage,
      this.list
      );

  factory HouseMemberListResult.fromJson(Map<String, dynamic> json) {
    return HouseMemberListResult(
      json['isSuccess'],
      json['code'],
      json['msg'],
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      json['list'] == null ? [] : (json['list'] as List).map((e) => HouseMemberNameList.fromJson(e)).toList()
    );
  }
}