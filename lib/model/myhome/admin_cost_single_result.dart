import 'package:app_sinbi_apartment/model/myhome/admin_cost_item.dart';

class AdminCostSingleResult {
  bool isSuccess;
  int code;
  String msg;
  AdminCostItem data;

  AdminCostSingleResult(
      this.isSuccess,
      this.code,
      this.msg,
      this.data
      );

  factory AdminCostSingleResult.fromJson(Map<String, dynamic> json) {
    return AdminCostSingleResult(
        json['isSuccess'],
        json['code'],
        json['msg'],
        AdminCostItem.fromJson(json['data'])
    );
  }
}