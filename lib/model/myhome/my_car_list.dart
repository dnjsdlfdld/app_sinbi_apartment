class MyCarList {
  String myCarNum;
  String carModel;
  String carType;

  MyCarList(
      this.myCarNum,
      this.carModel,
      this.carType
      );

  factory MyCarList.fromJson(Map<String, dynamic> json) {
    return MyCarList(
      json['myCarNum'],
      json['carModel'],
      json['carType']
    );
  }
}