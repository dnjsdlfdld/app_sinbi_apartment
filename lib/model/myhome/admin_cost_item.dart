class AdminCostItem {
  int costMonth;
  num price;

  AdminCostItem(
      this.costMonth,
      this.price
      );

  factory AdminCostItem.fromJson(Map<String, dynamic> json) {
    return AdminCostItem(
      json['costMonth'],
      json['price']
    );
  }
}