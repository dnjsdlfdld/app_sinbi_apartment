import 'package:app_sinbi_apartment/model/myhome/my_info_item.dart';

class MyInfoItemCommonResult {
  bool isSuccess;
  int code;
  String msg;
  MyInfoItem data;

  MyInfoItemCommonResult(
      this.isSuccess,
      this.code,
      this.msg,
      this.data
      );

  factory MyInfoItemCommonResult.fromJson(Map<String, dynamic> json) {
    return MyInfoItemCommonResult(
        json['isSuccess'],
        json['code'],
        json['msg'],
        MyInfoItem.fromJson(json['data'])
    );
  }
}