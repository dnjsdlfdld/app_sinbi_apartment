import 'package:app_sinbi_apartment/model/login/login_response.dart';

class LoginSingleResult {
  bool isSuccess;
  int code;
  String msg;
  LoginResponse data;

  LoginSingleResult(
      this.isSuccess,
      this.code,
      this.msg,
      this.data
      );

  factory LoginSingleResult.fromJson(Map<String, dynamic> json) {
    return LoginSingleResult(
        json['isSuccess'],
        json['code'],
        json['msg'],
        LoginResponse.fromJson(json['data'])
    );
  }
}